About Internet Corporation
--------------------------

This is a port of the Internet_Corporation theme by Luka Cvrk. This is a fixed width, 2 column layout.

Internet_Music requires the PHPTemplate theme engine. It is tested under Drupal 5.x, and valid XHTML 1.0 Strict.

Official Internet_Music theme project page:
  http://www.solucija.com/templates/demo/Internet_Corporation/


Ported by Ron Williams of Lithic Media - http://www.lithicmedia.com/