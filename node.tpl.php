<?php if ($teaser) { ?>
<h2><a href="<?php print $node_url ?>"><?php print $title ?></a></h2>
<h4><?php print $submitted ?></h4>
<p>
    <?php print $content ?>
    <?php if ($links): ?>
    <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
</p>
<?php } else { ?>
<h4><?php print $submitted ?></h4>
<p>
    <?php print $content ?>
    <?php if ($links): ?>
    <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
</p>
<?php } ?>